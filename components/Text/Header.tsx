import { styleClasses } from "../../styles/classes";

type Props = {
  children?: JSX.Element;
  text: string;
  variant: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
  classes?: string;
};

const Header = ({ children, variant, classes, ...props }: Props) => {
  const CustomHeader = variant;

  return (
    <CustomHeader
      {...props}
      className={`${classes ? classes : ""} ${styleClasses.text.mainHeader}`}
    >
      {props.text}
      {children}
    </CustomHeader>
  );
};

export default Header;
