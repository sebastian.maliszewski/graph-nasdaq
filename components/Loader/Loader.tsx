import { ScaleLoader } from "react-spinners";

const Loader = () => {
  return (
    <div className="loader w-screen h-screen fixed top-0 left-0 z-10 bg-[rgba(0,0,0,0.8)] flex flex-col justify-center items-center">
      <ScaleLoader color="#36d7b7" width={20} height={100} />
    </div>
  );
};

export default Loader;
