import Select from "react-select";

import { selectedCompaniesOptionInterface } from "../../types/main";

interface Props {
  handleSelectCompany: (
    selectedComopany: selectedCompaniesOptionInterface
  ) => void;
  companiesOptions: selectedCompaniesOptionInterface[];
  selectedComopany: selectedCompaniesOptionInterface;
}

const CustomSelect = ({
  handleSelectCompany,
  selectedComopany,
  companiesOptions,
}: Props) => {
  return (
    <div className="min-w-[300px] max-w-[500px]">
      <Select
        options={companiesOptions ? companiesOptions : []}
        onChange={(e: any) => handleSelectCompany(e)}
        value={selectedComopany}
      />
    </div>
  );
};

export default CustomSelect;
