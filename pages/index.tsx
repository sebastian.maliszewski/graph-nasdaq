import { useEffect, useState } from "react";
import moment from "moment";
import { usePromiseTracker, trackPromise } from "react-promise-tracker";

import {
  getQuandlCompanies,
  getSingleCompanyChartData,
  getSampleDataToRenderChart,
} from "../API/quandl";

import {
  companiesResponseInterface,
  selectedDatesInterface,
  selectedCompaniesOptionInterface,
  chartDataInterface,
} from "../types/main";

import Mainpage from "../views/Mainpage/Mainpage";

export default function Home() {
  const [nasdacCompanies, setNasdacCompanies] = useState<Array<Array<"">>>([]);
  const [selectedDates, setSelectedDates] = useState<selectedDatesInterface>({
    startDate: moment().subtract(60, "d").format("YYYY-MM-DD"),
    endDate: moment().format("YYYY-MM-DD"),
  });
  const [selectedCompanyCode, setSelectedCompanyCode] = useState("");
  const [selectedComopany, setSelectedCompany] =
    useState<selectedCompaniesOptionInterface>({
      value: "AAPL",
      label: "Apple Inc.",
    });
  const [companiesOptions, setCompaniesOptions] = useState<
    selectedCompaniesOptionInterface[]
  >([{ value: "", label: "" }]);
  const [chartData, setChartData] = useState<chartDataInterface[]>([]);
  const { promiseInProgress } = usePromiseTracker();

  // api data restrictions made me to apply default values in order to draw chart on the first render

  // if no companies - obtain them
  useEffect(() => {
    nasdacCompanies.length == 0 ? handleObtainQuandlCompanies() : null;
    handleObtainSampleDataToRenderChart();
  }, []);

  // if there are companies - make select component easy life with reading options
  useEffect(() => {
    nasdacCompanies?.length && handleExtractOptionsToSelect();
  }, [nasdacCompanies]);

  // if user selected some kind of optione - obtain data of that option
  useEffect(() => {
    selectedCompanyCode !== "" && handleObtainQuandlData();
  }, [selectedCompanyCode]);

  const handleObtainQuandlCompanies = async () => {
    const companies: companiesResponseInterface | any = await trackPromise(
      getQuandlCompanies()
    );

    companies &&
      companies.datatable &&
      setNasdacCompanies(companies.datatable.data);
  };

  const handleSelectCompany = (
    selectedCopmany: selectedCompaniesOptionInterface
  ) => {
    setSelectedCompanyCode(selectedCopmany.value);
    setSelectedCompany(selectedCopmany);
  };

  const handleExtractOptionsToSelect = () => {
    // cut response to 200 results to make it faster and easier to test
    const options: any = nasdacCompanies
      ?.filter((company, i) => i < 200)
      .map((company, i) => {
        return {
          value: company[0],
          label: company[2],
        };
      });

    setCompaniesOptions(options);
  };

  const handleObtainQuandlData = async () => {
    // I wasnt able to get data from Quandl API, so at first I used sample data (Apple) to render chart, but now I am using data from API (no data) and displaying announcement

    const chartData = await trackPromise(
      getSingleCompanyChartData(selectedCompanyCode)
    );
    chartData && handleExtractChartData(chartData);
  };

  const handleObtainSampleDataToRenderChart = async () => {
    const chartData: chartDataInterface | undefined = await trackPromise(
      getSampleDataToRenderChart()
    );

    chartData && handleExtractChartData(chartData);
    chartData &&
      setSelectedCompany({
        value: "AAPL",
        label: "Apple Inc.",
      });
  };

  const handleExtractChartData = (chartData: chartDataInterface) => {
    // 5 index in data array is close value index
    // 1 index in data array is date index

    const chartInfo: any = chartData.datatable.data.map((data: any) => {
      return {
        value: data[5],
        date: data[1],
      };
    });

    setChartData(chartInfo);
  };

  return (
    <div className="flex w-full">
      <Mainpage
        handleSelectCompany={handleSelectCompany}
        companiesOptions={companiesOptions}
        chartData={chartData}
        selectedComopany={selectedComopany}
        handleObtainSampleDataToRenderChart={
          handleObtainSampleDataToRenderChart
        }
        promiseInProgress={promiseInProgress}
      />
    </div>
  );
}
