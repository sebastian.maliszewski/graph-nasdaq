import {
  selectedCompaniesOptionInterface,
  chartDataInterface,
} from "../../types/main";

import Select from "../../components/Select/Select";
import Header from "../../components/Text/Header";
import ChartView from "./components/ChartView";
import Loader from "../../components/Loader/Loader";

type Props = {
  children?: JSX.Element;
  handleSelectCompany: (
    selectedComopany: selectedCompaniesOptionInterface
  ) => void;
  companiesOptions: selectedCompaniesOptionInterface[];
  chartData: chartDataInterface[];
  selectedComopany: selectedCompaniesOptionInterface;
  handleObtainSampleDataToRenderChart: () => void;
  promiseInProgress: boolean;
};

const Mainpage = ({
  handleSelectCompany,
  companiesOptions,
  chartData,
  selectedComopany,
  handleObtainSampleDataToRenderChart,
  promiseInProgress,
}: Props) => {
  return (
    <div className="flex flex-col justify-center items-center w-full">
      <Header classes="mt-6" variant="h1" text={"Historical trend"}></Header>
      <Select
        handleSelectCompany={handleSelectCompany}
        companiesOptions={companiesOptions}
        selectedComopany={selectedComopany}
      />
      <ChartView
        data={chartData}
        handleObtainSampleDataToRenderChart={
          handleObtainSampleDataToRenderChart
        }
      />
      {promiseInProgress && <Loader />}
    </div>
  );
};

export default Mainpage;
