import {
  XAxis,
  Tooltip,
  CartesianGrid,
  Line,
  LineChart,
  ResponsiveContainer,
  Area,
  AreaChart,
} from "recharts";

import { chartDataInterface } from "../../../types/main";

import { IoReloadOutline } from "react-icons/io5";
interface Props {
  data: chartDataInterface[];
  handleObtainSampleDataToRenderChart: () => void;
}

const ChartView = ({ data, handleObtainSampleDataToRenderChart }: Props) => {
  return (
    <div className="flex justify-center w-full mt-[10%] relative">
      <ResponsiveContainer width="90%" aspect={16.0 / 5.0}>
        <AreaChart
          height={550}
          data={data}
          margin={{
            top: 0,
            right: 30,
            left: 30,
            bottom: 35,
          }}
        >
          <defs>
            <linearGradient id="colorArea" x1="0" y1="0" x2="0" y2="1">
              <stop offset="30%" stopColor="#E6DADA" stopOpacity={0.5} />
              <stop offset="100%" stopColor="#EFF5F5" stopOpacity={0} />
            </linearGradient>
          </defs>
          <XAxis
            interval={0}
            width={20}
            height={10}
            dataKey="date"
            angle={-60}
          />
          <CartesianGrid stroke="#65647C" strokeDasharray="3 3" />
          <Tooltip contentStyle={{ backgroundColor: "#65647C" }} />
          <Area
            type="monotone"
            dataKey="value"
            stroke="#EFF5F5"
            fillOpacity={0.8}
            fill="url(#colorArea)"
          />
        </AreaChart>
      </ResponsiveContainer>

      {data.length == 0 && (
        <div className="absolute top-[calc(50%-60px)] h-[60px] w-full flex flex-col justify-center items-center gap-3">
          <div className=" flex justify-center items-center shadow-lg  border rounded-lg lg:p-4 p-2 border-[rgba(102,102,102,0.2)] bg-white text-sm lg:text-xl font-bold uppercase">
            No data available
          </div>
          <div
            onClick={() => handleObtainSampleDataToRenderChart()}
            className=" flex justify-center items-center shadow-lg  border rounded-lg lg:p-4 p-2 border-[rgba(102,102,102,0.2)] bg-white cursor-pointer hover:scale-105 hover:shadow-xl transition-all text-xs lg:text-lg"
          >
            <IoReloadOutline className=" mr-2 font-bold " />
            <span>Reload to obtain sample data</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default ChartView;
