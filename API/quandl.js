import axios from "axios";

export const getSingleCompanyChartData = async (quandlCode) => {
  let singleChartData;

  const getSingleCompanyChartDataConfig = {
    method: "GET",
    url: `${process.env.NEXT_PUBLIC_QUANDL_URL}/datatables/QUOTEMEDIA/PRICES`,
    headers: {
      "Content-Type": "application/json",
    },
    params: {
      api_key: process.env.NEXT_PUBLIC_QUANDL_API_KEY,
      ticker: quandlCode,
    },
  };

  await axios(getSingleCompanyChartDataConfig).then((response) => {
    singleChartData = response.data;
  });

  return singleChartData;
};

export const getQuandlCompanies = async () => {
  let companies;

  const getQuandlDatabasesConfig = {
    method: "GET",
    url: `${process.env.NEXT_PUBLIC_QUANDL_URL}/datatables/QUOTEMEDIA/TICKERS`,
    headers: {
      "Content-Type": "application/json",
    },
    params: {
      api_key: process.env.NEXT_PUBLIC_QUANDL_API_KEY,
      exchange: "NASDAQ",
    },
  };

  await axios(getQuandlDatabasesConfig).then((response) => {
    companies = response.data;
  });

  return companies;
};

export const getSampleDataToRenderChart = async () => {
  let chartData;

  const getQuandlDatabasesConfig = {
    method: "GET",
    url: `${process.env.NEXT_PUBLIC_QUANDL_URL}/datatables/QUOTEMEDIA/PRICES`,
    headers: {
      "Content-Type": "application/json",
    },
    params: {
      api_key: process.env.NEXT_PUBLIC_QUANDL_API_KEY,
      ticker: "AAPL",
    },
  };

  await axios(getQuandlDatabasesConfig).then((response) => {
    chartData = response.data;
  });

  return chartData;
};
