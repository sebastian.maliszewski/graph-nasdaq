export interface dataTableInterface {
  data: Array<Array<"">>;
  columns: Array<{ name: string; type: string }>;
}

export interface companiesResponseInterface {
  datatable: dataTableInterface;
  meta: { next_cursor_id: string | null };
}

export interface selectedDatesInterface {
  startDate: string;
  endDate: string;
}

export interface selectedCompaniesOptionInterface {
  value: string;
  label: string;
}

export interface chartDataInterface {
  datatable: dataTableInterface;
}

export interface chartDataInterface {
  date: string;
  value: number;
}
